package org.fasttrackit;

public class ProductNumber {
    public void clickOnProductNumber(){
        System.out.println("Click On Product Number");
    }
    public void userCanSeeTheProductPriceInDifferentCurrencies(){
        System.out.println("Expected Results: User can see the Product Price in different currencies");
    }
    public void userCanAddAsManyProductsAsHeWants(){
        System.out.println("Expected Results: User Can Add As Many Products As He Wants");
    }
}
