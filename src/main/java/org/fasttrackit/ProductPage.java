package org.fasttrackit;

public class ProductPage {
    public void openAwesomeGraniteChipsProductPage(){
        System.out.println("Open Awesome Granite Chips Product Page");
    }
    public void clickOnCartButton(){
        System.out.println("Click On The Cart Button");
    }
    public void clickOnTheProductPage(){
        System.out.println("Click On The Product Page");
    }
    public void userCanUnderstandProductDescription(){
        System.out.println("Expected Results: User can understand Product Description");
    }
    public void openAwesomeMetalChairProductPage(){
        System.out.println("Open Awesome Metal Chair Product Page");
    }
}
