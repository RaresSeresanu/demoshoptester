package org.fasttrackit;

public class Main {
    public static void main(String[] args) {
        System.out.println(" ---   DemoTester  --- ");
        System.out.println("1. User can login with valid credentials.");
        HomePage homepage = new HomePage();
        homepage.openHomePage();
        Header header = new Header();
        header.clickOnTheLoginButton();
        ModalDialog modal = new ModalDialog();
        modal.typeInUsername("Beetle");
        modal.typeInPassword("ChooChoo");
        modal.clickOnTheLoginButton();
        String greetingsMessage = header.getGreetingsMessage();
        boolean isLogged = greetingsMessage.contains("Beetle");
        System.out.println("Hi Beetle is dislayed in the header:" + isLogged);
        System.out.println("Expected Results: Greetings msg is " + greetingsMessage);
        System.out.println("-----------------------------------------------------------");

        System.out.println("2.User's credentials are secured and invisible");
        homepage.openHomePage();
        header.clickOnTheLoginButton();
        modal.typeInUsername("Beetle");
        modal.typeInPassword("ChooChoo");
        modal.clickOnTheLoginButton();
        QuestionButton questionButton = new QuestionButton();
        questionButton.goToQuestionButton();
        questionButton.usersCredentialsAreSecured();
        System.out.println("------------------------------------------------------------");

        System.out.println("3. User can Login with all recommended credentials ");
        homepage.openHomePage();
        ModalDialog2 modalDialog2 = new ModalDialog2();
        modalDialog2.clickOnTheLoginButton();
        modalDialog2.typeInUsername("locked");
        modalDialog2.typeInPassword("choochoo");
        modalDialog2.userCanLoginWithCredentials();
        System.out.println("-------------------------------------------------------------");

        System.out.println("4. User can see the Product Pictures on the Product Page");
        homepage.openHomePage();
        header.loginWithCredentials();
        ProductPage productPage = new ProductPage();
        productPage.openAwesomeGraniteChipsProductPage();
        ProductPicture productPicture = new ProductPicture();
        productPicture.clickOnTheProductPicture();
        productPicture.userCanSeeProductPicture();
        System.out.println("-----------------------------------------------------------");

        System.out.println("5. User can understand Product Description on the Product Page");
        homepage.openHomePage();
        header.loginWithCredentials();
        productPage.openAwesomeGraniteChipsProductPage();
        ProductDescription productDescription = new ProductDescription();
        productDescription.clickOnTheProductDescription();
        productPage.userCanUnderstandProductDescription();
        System.out.println("-----------------------------------------------------------");

        System.out.println("6. User can see all Product Pictures");
        homepage.openHomePage();
        header.loginWithCredentials();
        productPage.openAwesomeGraniteChipsProductPage();
        productPicture.clickOnTheProductPicture();
        productPicture.clickOnTheRefreshButton();
        productPicture.userCanSeeProductPicture();
        System.out.println("----------------------------------------------------------");

        System.out.println("7. User can see the same ColorFrame on Product Picture");
        homepage.openHomePage();
        header.loginWithCredentials();
        productPage.openAwesomeGraniteChipsProductPage();
        productPicture.clickOnTheProductPicture();
        productPicture.clickOnTheRefreshButton();
        productPicture.userCanSeeTheSameColourFrameOnProductPage();
        System.out.println("----------------------------------------------------------");

        System.out.println("8. User can see Different ColorFrame on Product Picture");
        homepage.openHomePage();
        header.loginWithCredentials();
        productPage.openAwesomeMetalChairProductPage();
        productPicture.clickOnTheProductPicture();
        productPicture.userCanSeeDifferentColorFrameOnProductPicture();
        System.out.println("----------------------------------------------------------");

        System.out.println("9. User can see the Correct Products Prices on the Cart Page");
        homepage.openHomePage();
        header.loginWithCredentials();
        CartPage cartPage = new CartPage();
        cartPage.addMultipleProductsToTheCart();
        cartPage.goToCartPage();
        cartPage.clickOnTheProductsPrices();
        cartPage.userCanSeeTheCorrectProductsPricesOnTheCartPage();
        System.out.println("----------------------------------------------------------");

        System.out.println("10. User can see the Correct Final Price of the Products from Cart");
        homepage.openHomePage();
        header.loginWithCredentials();
        cartPage.add15ProductsToCart();
        cartPage.goToCartPage();
        cartPage.clickOnTheProductsPrices();
        cartPage.userCanSeeTheCorrectProductsPricesOnTheCartPage();
        System.out.println("----------------------------------------------------------");

        System.out.println("11. User Can See All The Products Added To Cart");
        homepage.openHomePage();
        header.loginWithCredentials();
        cartPage.add10ProductsToCart();
        cartPage.goToCartPage();
        cartPage.clickOnTheProductsList();
        cartPage.userCanSeeAllTheProductsAddedToCart();
        System.out.println("-----------------------------------------------------------");

        System.out.println("12. User Can Add As Many Products As He Wants");
        homepage.openHomePage();
        header.loginWithCredentials();
        productPage.clickOnTheProductPage();
        productPage.clickOnCartButton();
        cartPage.goToCartPage();
        ProductNumber productNumber = new ProductNumber();
        productNumber.clickOnProductNumber();
        productNumber.userCanAddAsManyProductsAsHeWants();
        System.out.println("-----------------------------------------------------------");

        System.out.println("13. User Can See The Product Price in Different Currencies");
        homepage.openHomePage();
        header.loginWithCredentials();
        cartPage.addMultipleProductsToTheCart();
        cartPage.goToCartPage();
        cartPage.clickOnTheCurrencySymbol();
        productNumber.userCanSeeTheProductPriceInDifferentCurrencies();
        System.out.println("-----------------------------------------------------------");

        System.out.println("14. User can choose Different Payment Methods");
        homepage.openHomePage();
        header.loginWithCredentials();
        cartPage.addMultipleProductsToTheCart();
        cartPage.goToCartPage();
        cartPage.clickOnTheCheckOutButton();
        cartPage.userCanChooseDifferentPaymentMethods();
        System.out.println("-----------------------------------------------------------");

        System.out.println("15. User can choose Different Delivery Service.");
        homepage.openHomePage();
        header.loginWithCredentials();
        cartPage.addMultipleProductsToTheCart();
        cartPage.goToCartPage();
        cartPage.clickOnTheCheckOutButton();
        cartPage.clickOnThePaymentMethod();
        cartPage.userCanChooseDifferentPaymentMethods();
        System.out.println("----------------------------------------------------------");

        System.out.println("16. User can Check The State of his Delivery");
        homepage.openHomePage();
        header.loginWithCredentials();
        cartPage.goToCartPage();
        cartPage.clickOnDeliveryInformation();
        cartPage.clickOnStatusInformation();
        cartPage.userCanCheckTheStateOfHisDelivery();
        System.out.println("----------------------------------------------------------");

        System.out.println("17. User can receive notifications by Email");
        homepage.openHomePage();
        header.loginWithCredentials();
        Username username = new Username();
        username.clickOnTheUsernameField();
        username.clickOnNotificationsButton();
        username.userIsReceivingNotificationsByEmail();
        System.out.println("----------------------------------------------------------");

        System.out.println("18. User can receive offers from Demo Shop by Email");
        homepage.openHomePage();
        header.loginWithCredentials();
        username.clickOnTheUsernameField();
        username.clickOnTheOffersButton();
        username.userIsReceivingOffersFromDemoShop();
        System.out.println("---------------------------------------------------------");

        System.out.println("19. User can see Favorite Products after Refreshing the Page");
        homepage.openHomePage();
        header.loginWithCredentials();
        FavoriteProducts favoriteProducts = new FavoriteProducts();
        favoriteProducts.clickOnFavoriteButtonOnMultipleProducts();
        favoriteProducts.clickOnFavoritePage();
        favoriteProducts.clickOnTheRefreshButton();
        favoriteProducts.userCanSeeFavoriteProductsAfterRefresh();
        System.out.println("--------------------------------------------------------");

        System.out.println("20. User can use Search Bar to find products from the website");
        homepage.openHomePage();
        header.loginWithCredentials();
        SearchBar searchBar = new SearchBar();
        searchBar.clickOnTheSearchBar();
        searchBar.typeProductNameOnSearchBar();
        searchBar.userCanUseSearchBarToFindProductsFromTheWebsite();
        System.out.println("-------------------------------------------------------");

        System.out.println("21.  User can stay Logged in after any operation");
        homepage.openHomePage();
        header.loginWithCredentials();
        cartPage.addMultipleProductsToTheCart();
        cartPage.goToCartPage();
        cartPage.clickOnRefreshButton();
        cartPage.userCanStayLoggedInAfterAnyOperation();
        System.out.println("-------------------------------------------------------");



    }
}