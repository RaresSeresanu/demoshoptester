package org.fasttrackit;

public class ProductPicture {
    public void clickOnTheProductPicture(){
        System.out.println("Click on the Product Picture");
    }
    public void clickOnTheNextPicture(){
        System.out.println("Click on the Next Picture");
    }
    public void clickOnTheRefreshButton(){
        System.out.println("Click on the Refresh Button");
    }
    public void userCanSeeProductPicture(){
        System.out.println("Expected Results: User can See Product Picture");
    }
    public void userCanSeeTheSameColourFrameOnProductPage(){
        System.out.println("Expected Results: User can see the same ColourFrame");
    }
    public void userCanSeeDifferentColorFrameOnProductPicture(){
        System.out.println("Expected Results: User can see Different ColorFrame on Product Picture");
    }
}
