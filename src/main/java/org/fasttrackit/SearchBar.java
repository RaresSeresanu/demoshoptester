package org.fasttrackit;

public class SearchBar {
    public void clickOnTheSearchBar(){
        System.out.println("Click on the Search Bar");
    }
    public void typeProductNameOnSearchBar(){
        System.out.println("Type Product Name on Search Bar");
    }
    public void userCanUseSearchBarToFindProductsFromTheWebsite(){
        System.out.println("Expected Results: User can use Search Bar to find products from the website");
    }
}
