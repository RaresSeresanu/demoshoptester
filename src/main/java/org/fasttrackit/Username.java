package org.fasttrackit;

public class Username {
    public void clickOnTheUsernameField(){
        System.out.println("Click on the Username Field");
    }
    public void clickOnNotificationsButton(){
        System.out.println("Click on Notifications Button");
    }
    public void userIsReceivingNotificationsByEmail(){
        System.out.println("Expected Results: User is receiving Notifications by Email");
    }
    public void clickOnTheOffersButton(){
        System.out.println("CLick on the Offers Button");
    }
    public void userIsReceivingOffersFromDemoShop(){
        System.out.println("Expected Results: User is receiving Offers from Demo Shop");
    }
}
