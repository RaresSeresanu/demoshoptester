package org.fasttrackit;

public class FavoriteProducts {
    public void clickOnFavoriteButtonOnMultipleProducts(){
        System.out.println("Click on Favorite Button on Multiple Products");
    }
    public void clickOnFavoritePage(){
        System.out.println("Click on the Favorite Page");
    }
    public void clickOnTheRefreshButton(){
        System.out.println("Click on the Refresh Button");
    }
    public void userCanSeeFavoriteProductsAfterRefresh(){
        System.out.println("Expected Results: User can see Favorite Products after Refresh the Page");
    }
}
