package org.fasttrackit;

public class CartPage {
    public void addMultipleProductsToTheCart(){
        System.out.println("Add Multiple Products To The Cart");
    }
    public void goToCartPage(){
        System.out.println("Go to Cart Page");
    }
    public void clickOnTheProductsPrices(){
        System.out.println("Click On The Products Prices");
    }
    public void add15ProductsToCart(){
        System.out.println("Add 15 Products To Cart");
    }
    public void add10ProductsToCart(){
        System.out.println("Add 10 Products To Cart");
    }
    public void clickOnTheProductsList(){
        System.out.println("Click On The Product List");
    }
    public void clickOnTheCurrencySymbol(){
        System.out.println("Click On The Currency Symbol");
    }
    public void clickOnTheCheckOutButton(){
            System.out.println("Click on The CheckOut Button");
    }
    public void clickOnThePaymentMethod(){
            System.out.println("Click on The Payment Method");
        }
    public void clickOnDeliveryInformation(){
        System.out.println("Click on Delivery Information");
    }
    public void userCanSeeTheCorrectProductsPricesOnTheCartPage(){
        System.out.println("Expected Results: User can see the Correct Price on the Cart Page");
    }
    public void clickOnStatusInformation(){
        System.out.println("Click on Status Information");
    }
    public void userCanCheckTheStateOfHisDelivery(){
        System.out.println("Expected Results: User can Check the State of his Delivery");
    }
    public void clickOnRefreshButton(){
        System.out.println("Click on Refresh Button");
    }
    public void userCanStayLoggedInAfterAnyOperation(){
        System.out.println("Expected Results: User can stay Logged In after any Operation");
    }
    public void userCanSeeAllTheProductsAddedToCart(){
        System.out.println("Expected Results: User can see all the Products added to Cart");
    }
    public void userCanChooseDifferentPaymentMethods(){
        System.out.println("Expected Results: User can choose Different Payment Methods");
    }
}
